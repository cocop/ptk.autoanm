import wx

from gui import models, views

wx_app = wx.App()
window_model = models.Window()
window = views.Window(window_model)

window.Show()
wx_app.MainLoop()
