import os

from module import analyze, load

psd_path = load.get_psd_path()
profile_path = load.get_profile_path(psd_path)

try:
    print("出力を生成しています...。")
    an = analyze.Analyzer(psd_path)
    folder = an.analyze(profile_path)

    print("------------------------------------")
    print("出力します。\n")
    load.out_anm(folder, psd_path)
    print("正常に終了しました。")
except Exception as e:
    print("エラーが発生しました。\n")
    print(e)
finally:
    os.system('PAUSE')
