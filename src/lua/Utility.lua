local PTKAutoAnm = {}


function PTKAutoAnm:addstates(values, index, truck)
  if PSD.valueholder ~= nil then
    index = PSD.valueholder:get(index, PSD.valueholderindex + truck, 0)
  end

  if type(values[index]) == "table" and getmetatable(values[index]) == nil then
    for i, v in ipairs(values[index]) do
      PSD:addstate(v)
    end
  else
    PSD:addstate(values[index])
  end
end


function PTKAutoAnm:register(new_offset)
  PSD.valueholderindex = new_offset + PSD.valueholderindex
end


function PTKAutoAnm:anime(values, offset, speed, flip, loop, interval)
  if flip and 3 <= #values  then
    local len = #values
    for i = 1, len - 2 do
      table.insert(values, #values + 1, values[len - i])
    end
  end

  local index = offset
  local ani_frm = obj.frame
  local ani_len = speed * #values

  if PSD.valueholder ~= nil then
    ani_frm = PSD.valueholder.frame
  end

  if loop then
    ani_frm = ani_frm % (interval + ani_len)
  end

  if ani_frm < ani_len then
    index = (offset - 1 + math.floor(ani_frm / speed)) % #values + 1
  end 

  return values[index]
end


return PTKAutoAnm