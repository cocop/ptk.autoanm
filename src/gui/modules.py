import json
from pathlib import Path
from typing import Callable

from module import layer


def withValueHandler(value, handler: Callable):
    def Handler(e):
        return handler(e, value)
    return Handler


class AllRawLayerReceiver(layer.Receiver[list[list[str]]]):
    def __init__(self):
        self.result: list[list[str]] = []

    def catch(self, path: list[str]) -> layer.NextCheck:
        self.result.append(path)
        return layer.NextCheck.Next

    def get_result(self):
        return self.result
