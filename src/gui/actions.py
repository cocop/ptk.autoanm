import abc
import json
from typing import Callable, Generic, Optional, TypeVar

import jsonschema
import pyperclip
from module import analyze, load

from gui.context import Context

T = TypeVar('T')


class Action(abc.ABC, Generic[T]):
    @abc.abstractmethod
    def run(self, param: T):
        pass


class Multiple(Action[T], Generic[T]):
    def __init__(self, actions: list[Action[T]]) -> None:
        self._actions = actions

    def run(self, param: T):
        for act in self._actions:
            act.run(param)


class Notice(Action[T], Generic[T]):
    def __init__(self, notify: Optional[Callable[[T], None]] = None) -> None:
        self.notify = notify

    def run(self, param: T):
        if self.notify is None:
            return

        self.notify(param)


class LoadFile(Action[str]):
    def __init__(self, ctx: Context, notify_msg: Optional[Callable[[str], None]] = None) -> None:
        self._ctx = ctx
        self.notify_msg = notify_msg

    def run(self, path: str):
        try:
            (psd_path, profile_path) = load.get_valid_file_path(path)

            if psd_path is None:
                self._notify_msg("対応するPSDファイルが見つかりませんでした")
                return

            if profile_path is None:
                self._notify_msg("対応するプロファイルが見つかりませんでした")
                return

            self._ctx.analyzer = analyze.Analyzer(psd_path)
            self._ctx.diff_info = self._ctx.analyzer.analyze(profile_path)
            self._notify_msg("プロファイルを読み込みました")
        except jsonschema.ValidationError as e:
            self._notify_msg("\n".join([
                "プロファイルが正しい形式ではありませんでした",
                str(e)
            ]))
        except json.JSONDecodeError as e:
            self._notify_msg("\n".join([
                "プロファイルをデコード出来ませんでした",
                str(e)
            ]))
        except Exception as e:
            self._notify_msg("\n".join([
                "エラーが発生しました",
                str(e)
            ]))

    def _notify_msg(self, msg: str):
        if self.notify_msg is None:
            return
        self.notify_msg(msg)


class CopyRuleToClipboard(Action[list[list[str]]]):
    def run(self, path_list: list[list[str]]):
        text = ",\n".join(
            [
                "".join(
                    [
                        "^", "/".join(path), "/\\\\*({@})$"
                    ])
                for path in path_list
            ])
        pyperclip.copy(text)
