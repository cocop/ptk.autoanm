import pathlib
from typing import Optional, Tuple

import wx
from module import anm, watch
from watchdog import observers

from gui import actions, context

from . import modules, views


class LayerTree(views.LayerTreeModel):
    def __init__(self):
        self._path_list: Optional[list[list[str]]] = None
        self.notify_copy_rule: Optional[actions.Action[list[list[str]]]] = None

    def as_view(self) -> bool:
        return not self._path_list is None

    def get_path_list(self):
        if self._path_list is None:
            raise ValueError("self._path_list is None")

        return self._path_list

    def set_path_list(self, path_list: list[list[str]]):
        self._path_list = path_list

        if self.view_update is None:
            return

        self.view_update()

    def copy_rule(self, path_list: list[list[str]]):
        if not self.notify_copy_rule is None:
            self.notify_copy_rule.run(path_list)


class TruckInfo(views.TruckInfoModel):
    def __init__(self):
        self._diff_info: Optional[anm.struct.AnmFolder] = None
        self.notify_change_truck: Optional[actions.Action[Tuple[int, int, int]]] = None

    def as_view(self) -> bool:
        return not self._diff_info is None

    def set_diff_info(self, diff_info: anm.struct.AnmFolder):
        self._diff_info = diff_info
        if not self.view_update is None:
            self.view_update()

    def get_diff_info(self):
        if self._diff_info is None:
            raise ValueError("self._diff_info is None")

        return self._diff_info

    def change_truck(self, file_idx: int, truck_idx: int, value_idx: int):
        if self.notify_change_truck is None:
            return
        self.notify_change_truck.run((file_idx, truck_idx, value_idx))


class LogInfo(views.LogInfoModel):
    def __init__(self) -> None:
        self.logs: list[str] = []

    def add_log(self, log: str):
        self.logs.append(log)

        if not self.view_update is None:
            self.view_update()

    def as_view(self):
        return len(self.logs) != 0

    def get_log(self) -> list[str]:
        return self.logs


class TrackValueInfo(views.TrackValueInfoModel):
    def __init__(self) -> None:
        self.track_value: Optional[anm.struct.TrackPosition] = None

    def as_view(self) -> bool:
        return self.track_value != None and self.opt != None

    def get_track_value_info(self) -> str:
        if self.track_value is None or self.opt is None:
            raise ValueError("self.track_value is None or self.opt is None")

        return self.track_value.get_psdtoolkit_str(self.opt, 2, 0)

    def set_track_value(self, track_value: anm.struct.TrackPosition, opt: anm.struct.ToPTKStrOption):
        self.track_value = track_value
        self.opt = opt
        if not self.view_update is None:
            self.view_update()


class Window(views.WindowModel):
    def __init__(self):
        self._app_ctx = context.Context()
        self._observer: Optional[observers.Observer] = None

        self.layer_tree: LayerTree = LayerTree()
        self.truck_info: TruckInfo = TruckInfo()
        self.log_info: LogInfo = LogInfo()
        self.track_value_info: TrackValueInfo = TrackValueInfo()

        self.notify_load_file = actions.Multiple([
            actions.LoadFile(self._app_ctx, self.log_info.add_log),
            actions.Notice[str](self._loaded_file)])

        self.layer_tree.notify_copy_rule = actions.CopyRuleToClipboard()

        self.truck_info.notify_change_truck = actions.Notice(
            self._change_truck)

    def _change_truck(self, param: Tuple[int, int, int]):
        (file_idx, truck_idx, value_idx) = param

        if value_idx == 0:
            return

        if self._app_ctx.diff_info is None:
            return

        file = self._app_ctx.diff_info.anm_files[file_idx]
        tracks = sorted(file.tracks, key=lambda x: x.index)

        self.track_value_info.set_track_value(
            tracks[truck_idx].values[value_idx - 1],
            file.opt)

    def _loaded_file(self, path: str):
        if self._app_ctx.analyzer is None or self._app_ctx.diff_info is None:
            return

        if not self._observer is None:
            self._observer.stop()

        self._observer = observers.Observer()
        self._observer.schedule(watch.FileWatchingHandler(self.load_file, pathlib.Path(path).stem),
                                pathlib.Path(path).parent,
                                recursive=False)
        self._observer.start()

        path_list = self._app_ctx.analyzer.checker.check(
            modules.AllRawLayerReceiver())
        self.layer_tree.set_path_list(path_list)
        self.truck_info.set_diff_info(self._app_ctx.diff_info)

    def as_view(self) -> bool:
        return True

    def load_file(self, path: str):
        wx.CallAfter(self.notify_load_file.run, path)
