from abc import ABC, abstractmethod
from typing import Any, Callable, Optional

import wx
import wx.lib.scrolledpanel
from module import anm

from gui.modules import withValueHandler

# ============================================


def createDataWithEventHandler(handler: Callable, data: Any):
    def event(e):
        return handler(e, data)
    return event


# ============================================


class ViewModel(ABC):
    @abstractmethod
    def as_view(self) -> bool:
        pass

    view_update: Optional[Callable] = None


# ============================================


class LayerTreeModel(ViewModel):
    @abstractmethod
    def get_path_list(self) -> list[list[str]]:
        pass

    @abstractmethod
    def copy_rule(self, path_list: list[list[str]]):
        pass


class LayerTree(wx.Panel):
    def __init__(self, model: LayerTreeModel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = model
        model.view_update = self._view_update

        layout = wx.BoxSizer(wx.VERTICAL)

        # --------------------
        self._tc = wx.TreeCtrl(self,
                               style=wx.TR_DEFAULT_STYLE
                               | wx.TR_NO_LINES
                               | wx.TR_HIDE_ROOT
                               | wx.TR_HAS_BUTTONS
                               | wx.TR_TWIST_BUTTONS
                               | wx.TR_NO_LINES
                               | wx.TE_MULTILINE
                               | wx.TR_FULL_ROW_HIGHLIGHT)
        self._tc.SetBackgroundColour(self.GetBackgroundColour())
        self.Bind(wx.EVT_TREE_ITEM_RIGHT_CLICK, self._show_menu)

        # --------------------
        self._menu = wx.Menu()
        self._copy_rule_id = self._menu.Append(wx.MenuItem(
            self._menu,
            wx.ID_ANY,
            text="このレイヤー内を差分とするルールをクリップボードにコピーする")).Id

        self.Bind(wx.EVT_MENU, self._select_menu)

        # --------------------

        layout.Add(self._tc, flag=wx.GROW, proportion=1)
        self.SetSizer(layout)

        self._view_update()

    def _view_update(self, *args, **kwargs):
        if not self._model.as_view():
            return

        self._tc.DeleteAllItems()

        root: wx.TreeItemId = self._tc.AddRoot("root")
        self._tree_setup(root, 0, 0, self._model.get_path_list())

    def _tree_setup(self, host: wx.TreeItemId, depth: int, index: int, path_list: list[list[str]]):
        path_len = len(path_list)
        now_id: wx.TreeItemId = host

        while index < path_len:
            depth_should = len(path_list[index]) - 1

            if depth == depth_should:
                now_id = self._tc.AppendItem(
                    host,
                    path_list[index][depth],
                    data=path_list[index])
            elif depth < depth_should:
                index = self._tree_setup(now_id, depth + 1, index, path_list)
                continue
            else:
                return index

            index += 1

    def _show_menu(self, e):
        self.PopupMenu(self._menu)

    def _select_menu(self, e):
        {
            self._copy_rule_id: self._copy_rule,
        }[e.Id]()

    def _copy_rule(self):
        selected_data_list = map(lambda x: self._tc.GetItemData(x),
                                 self._tc.GetSelections())

        self._model.copy_rule(list(selected_data_list))

# ============================================


class TruckInfoModel(ViewModel):
    @ abstractmethod
    def get_diff_info(self) -> anm.struct.AnmFolder:
        pass

    @ abstractmethod
    def change_truck(self, file_idx: int, truck_idx: int, value_idx: int):
        pass


class SmallSlider(wx.Panel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._slider = wx.Slider(self)
        self._min_label = wx.StaticText(self)
        self._max_label = wx.StaticText(self)
        self._val_label = wx.StaticText(self,
                                        size=(30, -1),
                                        style=wx.TE_CENTER)

        layout = wx.BoxSizer()
        layout.Add(self._slider)
        layout.Add(self._min_label)
        layout.Add(self._val_label)
        layout.Add(self._max_label)
        self.SetSizer(layout)

        self._slider.Bind(wx.EVT_SLIDER,
                          self._change_slider_value,
                          self._slider)
        self.change_value: Optional[Callable[[SmallSlider], None]] = None

    def _change_slider_value(self, e):
        self._val_label.SetLabelText(str(self._slider.GetValue()))

        if not self.change_value == None:
            self.change_value(self)

    def set_min(self, min_value: int):
        if self._slider.GetMax() - min_value <= 0:
            self._slider.Disable()
        else:
            self._slider.Enable()
            self._slider.SetMin(min_value)

        self._min_label.SetLabelText(str(min_value) + " < ")

    def set_max(self, max_value: int):
        if max_value - self._slider.GetMin() <= 0:
            self._slider.Disable()
        else:
            self._slider.Enable()
            self._slider.SetMax(max_value)

        self._max_label.SetLabelText(" > " + str(max_value))

    def set_val(self, value: int):
        self._slider.SetValue(value)
        self._val_label.SetLabelText(str(value))

    def get_min(self) -> int:
        return self._slider.GetMin()

    def get_max(self) -> int:
        return self._slider.GetMax()

    def get_val(self) -> int:
        return self._slider.GetValue()


class TruckInfo(wx.lib.scrolledpanel.ScrolledPanel):
    def __init__(self, model: TruckInfoModel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = model
        self._model.view_update = self._view_update

    def _view_update(self):
        self.DestroyChildren()

        base_layout = wx.BoxSizer()
        content = wx.Panel(self)

        if not self._model.as_view():
            wx.StaticText(self,
                          label="プロファイルが読み込めませんでした")
            return

        anm_folder = self._model.get_diff_info()

        if anm_folder is None:
            return

        layout = wx.BoxSizer(wx.VERTICAL)
        layout.Add(wx.StaticText(content, label=anm_folder.name))

        for fi, file in enumerate(anm_folder.anm_files):
            layout.Add(wx.Panel(content, size=wx.Size(0, 16)))
            layout.Add(wx.StaticText(content, label=file.name))
            for ti, track in enumerate(sorted(file.tracks, key=lambda x: x.index)):
                layout.Add(wx.Panel(content, size=wx.Size(0, 8)))
                layout.Add(wx.StaticText(content, label=track.name))

                truck_slider = SmallSlider(content)
                truck_slider.set_min(1 if track.always_apply else 0)
                truck_slider.set_max(len(track.values))
                layout.Add(truck_slider)

                select_list = list(map(lambda x: x.name, track.values))
                if not track.always_apply:
                    select_list.insert(0, "none")
                truck_selector = wx.ComboBox(content,
                                             choices=select_list,
                                             style=wx.CB_READONLY)
                layout.Add(truck_selector)

                truck_slider.change_value = withValueHandler((fi, ti, truck_selector),
                                                             self._change_slider)
                truck_selector.Bind(wx.EVT_COMBOBOX,
                                    withValueHandler((fi, ti, truck_slider), self._change_selector))

                truck_slider.set_val(truck_slider.get_val())
                truck_selector.SetSelection(0)

        content.SetSizer(layout)
        base_layout.Add(content, flag=wx.GROW | wx.ALL, border=8)
        self.SetSizer(base_layout)
        self.SetupScrolling(False, True)

    def _change_slider(self, sender: SmallSlider, value: tuple[int, int, wx.ComboBox]):
        (fi, ti, selector) = value
        selector.SetSelection(sender.get_val() - sender.get_min())
        self._model.change_truck(fi, ti, sender.get_val())

    def _change_selector(self, e, value: tuple[int, int, SmallSlider]):
        (fi, ti, slider) = value
        selector: wx.ComboBox = e.GetEventObject()
        slider.set_val(selector.GetSelection() + slider.get_min())
        self._model.change_truck(fi, ti, slider.get_val())


# ============================================


class LogInfoModel(ViewModel):
    @abstractmethod
    def get_log() -> list[str]:
        pass


class LogInfo(wx.Panel):
    def __init__(self, model: LogInfoModel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = model
        self._model.view_update = self._view_update

        layout = wx.BoxSizer(wx.VERTICAL)
        self._log_text_ctrl = wx.TextCtrl(self,
                                          style=wx.TE_MULTILINE | wx.TE_READONLY)
        layout.Add(self._log_text_ctrl, flag=wx.GROW, proportion=1)
        self.SetSizer(layout)

    def _view_update(self):
        if not self._model.as_view():
            return

        self._log_text_ctrl.SetValue("\n".join(self._model.get_log()))


# ============================================


class TrackValueInfoModel(ViewModel):
    @abstractmethod
    def get_track_value_info(self) -> str:
        pass


class TrackValueInfo(wx.Panel):
    def __init__(self, model: TrackValueInfoModel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = model
        self._model.view_update = self._view_update

        layout = wx.BoxSizer(wx.VERTICAL)
        self._track_value_text_ctrl = wx.TextCtrl(self,
                                                  style=wx.TE_MULTILINE | wx.TE_READONLY)
        layout.Add(self._track_value_text_ctrl, flag=wx.GROW, proportion=1)
        self.SetSizer(layout)

    def _view_update(self):
        if not self._model.as_view():
            return

        self._track_value_text_ctrl.SetValue(
            self._model.get_track_value_info())


# ============================================


class WindowModel(ViewModel):
    layer_tree: LayerTreeModel
    truck_info: TruckInfoModel
    log_info: LogInfoModel
    track_value_info: TrackValueInfoModel

    @abstractmethod
    def load_file(self, path: str):
        pass


class Window(wx.Frame):
    def __init__(self, model: WindowModel, *args, **kwargs):
        super().__init__(None,
                         wx.ID_ANY,
                         "ProfilePreview",
                         size=(896, 672))
        self._model = model
        self._init_layout()

    def _init_layout(self):
        self.Centre()

        self._init_layout_menue()

        # ---- #
        window_layout = wx.BoxSizer(wx.HORIZONTAL)

        layer_tree = LayerTree(self._model.layer_tree,
                               self,
                               size=wx.Size(200, 200))

        truck_info = TruckInfo(self._model.truck_info,
                               self,
                               size=wx.Size(300, 300))

        # ---- #
        result_layout = wx.BoxSizer(wx.VERTICAL)
        result_info = wx.Panel(self)

        track_value_info = TrackValueInfo(self._model.track_value_info,
                                          result_info,
                                          size=(200, 200))
        track_value_info.SetMinSize(wx.Size(200, 200))
        track_value_info.SetBackgroundColour(wx.Colour(67, 89, 34))

        log_info = LogInfo(self._model.log_info,
                           result_info,
                           size=wx.Size(200, 200))
        log_info.SetMinSize(wx.Size(200, 200))

        result_layout.Add(track_value_info, flag=wx.GROW, proportion=3)
        result_layout.Add(log_info, flag=wx.GROW, proportion=2)
        result_info.SetSizer(result_layout)

        # ---- #
        window_layout.Add(layer_tree, flag=wx.GROW)
        window_layout.Add(truck_info, flag=wx.GROW)
        window_layout.Add(result_info, flag=wx.GROW, proportion=1)
        self.SetSizer(window_layout)

        # ---- #

    def _init_layout_menue(self):
        menu_bar = wx.MenuBar()
        menu_file = wx.Menu()
        self._load_id: wx.MenuItem = menu_file.Append(1, "開く").Id
        menu_bar.Append(menu_file, "ファイル")
        self.SetMenuBar(menu_bar)

        menu_bar.Bind(wx.EVT_MENU, self._select_menu)

    def _select_menu(self, e):
        if e.GetId() == self._load_id:
            self._load_file()
            return

    def _load_file(self):
        dialog = wx.FileDialog(None,
                               "psdファイルかプロファイルを選択してください。",
                               style=wx.FD_OPEN)
        dialog.SetWildcard("PSD or プロファイル (*.psd, *.json)|*.psd;*.json")

        if dialog.ShowModal() == wx.ID_OK:
            self._model.load_file(dialog.GetPath())
        dialog.Destroy()
