from typing import Optional

from module import analyze, anm


class Context:
    def __init__(self) -> None:
        self.analyzer: Optional[analyze.Analyzer] = None
        self.diff_info: Optional[anm.struct.AnmFolder] = None
