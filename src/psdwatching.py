import sys
from datetime import datetime, timedelta
from pathlib import Path

from watchdog.observers import Observer

from module import analyze, load, watch


def run_autoanm(path: str):
    (psd_path, profile_path) = load.get_valid_file_path(path)

    if psd_path is None or profile_path is None:
        return

    try:
        an = analyze.Analyzer(psd_path)
        folder = an.analyze(profile_path)

        print("------------------------------------")
        print("出力します。\n")
        load.out_anm(folder, psd_path)
        print("正常に終了しました。")
    except Exception as e:
        print("エラーが発生しました。\n")
        print(e)


observer = Observer()
h = watch.WatchingHandler(run_autoanm, ["*.psd", "*.json"])

watch_path = None
if len(sys.argv) >= 2:
    watch_path = load.del_quotation(sys.argv[1])

while(watch_path == None or watch_path == "" or not Path(watch_path).exists()):
    print("psdファイルがあるフォルダーのパスを入力してください。")
    watch_path = input("path:")

observer.schedule(h,
                  Path(watch_path).resolve(),
                  recursive=True)
observer.start()

while(True):
    h.check_update_list()
