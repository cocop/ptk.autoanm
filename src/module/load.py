import shutil
import sys
from pathlib import Path
from typing import Optional

from module.anm.struct import AnmFolder


def del_quotation(str: Optional[str]):
    if str == None or len(str) <= 2:
        return str

    if (str[0] == "'" and str[-1] == "'") or (str[0] == '"' and str[-1] == '"'):
        return str[1:-1]

    return str


def get_valid_file_path(path: str):
    psd_path = None
    profile_path = None
    p = Path(path)
    psd_candidacy = p.with_suffix(".psd")
    profile_candidacys = [p.with_suffix(".json"), p.with_name("profile.json")]

    if psd_candidacy.exists():
        psd_path = str(psd_candidacy)

    for profile_candidacy in profile_candidacys:
        if profile_candidacy.exists():
            profile_path = str(profile_candidacy)
            break

    return (psd_path, profile_path)


def get_arg_path(arg_idx: int):
    if arg_idx < len(sys.argv):
        path = sys.argv[arg_idx]
        if path != "" and Path(path).exists():
            return path


def get_input_path(input_msg: str, empty_file_msg: str):
    path = None
    while True:
        print(input_msg)
        path = del_quotation(input("path:"))
        if path == None:
            raise ValueError()

        print(path)
        if path != "" and Path(path).exists():
            return path
        print(empty_file_msg)


def get_psd_path():
    arg_path = get_arg_path(1)
    if arg_path != None:
        return arg_path

    return get_input_path("PSDファイルを指定してください。", "存在しないファイルです。")


def get_profile_path(psd_path: str):
    arg_path = get_arg_path(1)
    if arg_path != None:
        return arg_path

    _, profile_path = get_valid_file_path(psd_path)
    if profile_path != None:
        print(profile_path)
        return profile_path

    return get_input_path("プロファイルを指定してください。", "存在しないファイルです。")


def out_anm(folder: AnmFolder, psd_path: str):
    dist_folder = (folder.dist_path / Path(folder.name))
    dist_folder.mkdir(parents=True, exist_ok=True)

    psd_copy_path = folder.psd_copy_path
    if psd_copy_path != None:
        shutil.copy(psd_path, psd_copy_path)

    print(folder.name)

    for file in folder.anm_files:
        print("".join(["  ", file.name]))
        for track in file.tracks:
            print("".join(["  : ", track.name]))
            for value in track.values:
                print("".join(["    - ", value.name]))
        (dist_folder / file.name).write_text(file.get_psdtoolkit_str(2))
    (dist_folder / folder.utl_file.name).write_text(folder.utl_file.get_psdtoolkit_str(2))
