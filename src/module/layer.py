import abc
import enum
import typing

from psd_tools.api.layers import GroupMixin
from psd_tools.api.psd_image import PSDImage

T = typing.TypeVar('T')


class NextCheck(enum.Enum):
    Next = 1,
    DirSkip = 2,
    End = 3,


class Receiver(abc.ABC, typing.Generic[T]):
    @abc.abstractmethod
    def catch(self, path: list[str]) -> NextCheck:
        pass

    @abc.abstractmethod
    def get_result(self) -> T:
        pass


class Checker:
    def __init__(self, file: PSDImage) -> None:
        self.file = file

    def check(self, receiver: Receiver[T]) -> T:
        self._check_group_layer(receiver, self.file, [])
        return receiver.get_result()

    def _check_group_layer(self, receiver: Receiver, group_layer: GroupMixin, path: list[str]):
        for layer in reversed(list(group_layer)):
            new_path = path + [layer.name]

            if layer.name.endswith((":flipx", ":flipy", ":flipxy")):
                continue

            next_check = receiver.catch(new_path)

            if NextCheck.DirSkip == next_check:
                continue
            elif NextCheck.End == next_check:
                return NextCheck.End

            if not layer.is_group():
                continue

            if NextCheck.End == self._check_group_layer(receiver, layer, new_path):
                return NextCheck.End

        return NextCheck.Next
