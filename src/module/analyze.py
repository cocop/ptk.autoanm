import json
import pathlib
import sys
from functools import cmp_to_key
from typing import Callable, Optional

import jsonschema
from psd_tools.api.psd_image import PSDImage

from module.layer import Checker

from . import anm, rule

Comparison = Callable[[tuple[int, anm.struct.TrackPosition],
                       tuple[int, anm.struct.TrackPosition]], int]


class Sort:
    @staticmethod
    def get_tracks_order_index(track_names: list[str], order: Optional[list]):
        if order != None:
            return [order.index(track_name) for track_name in track_names]

        return [x for x in range(len(track_names))]

    @staticmethod
    def track_pos(comparison: Comparison, layer_checker: Checker, track_pos_list: list[anm.struct.TrackPosition]):
        layer_list = layer_checker.check(rule.AllLayerReceiver())

        sorted_items = sorted([(Sort._get_index(track_pos, layer_list), track_pos)
                               for track_pos in track_pos_list],
                              key=cmp_to_key(comparison))
        return list(map(lambda x: x[1], sorted_items))

    @staticmethod
    def _get_index(track_pos: anm.struct.TrackPosition, layer_list: list[str]):
        if len(track_pos.items) >= 0:
            return layer_list.index(track_pos.items[0].get_symbol_layer())

        return sys.maxsize


class RuleChecker:
    @staticmethod
    def check(layer_checker: Checker, rules: list[rule.Rule]):
        result: dict[str, list[anm.struct.Presenter]] = {}

        for rule_ in rules:
            if rule_.target == rule.Target.File:
                layer_checker.check(rule.RuleReceiver(result, rule_))
            elif rule_.target == rule.Target.Result:
                RuleChecker._check_result(result, rule_)

        return list(map(lambda x: anm.struct.TrackPosition(x[0], x[1]),
                        result.items()))

    @staticmethod
    def _check_result(result: dict[str, list[anm.struct.Presenter]], rule_: rule.Rule):
        for key in result.keys():
            hit_value = rule_.check(key)
            if hit_value != None:
                rule_.merger.merge(result, hit_value)


class Analyzer:
    def __init__(self, psd_path: str) -> None:
        self.psd_path = pathlib.Path(psd_path)
        self.checker = Checker(PSDImage.open(psd_path))

    def analyze(self, profile_path: str) -> anm.struct.AnmFolder:
        profile = self._get_profile(profile_path)

        opt = anm.struct.ToPTKStrOption(
            "",  # あとから設定する
            profile["layer_prefix"],
            "ptk",
            "utl")

        anm_folder = anm.struct.AnmFolder(
            profile["name"].replace("{@}", self.psd_path.stem),
            profile.get("psd_copy_path"),
            profile.get("dist_path", ""),
            list(map(lambda x: self._anm_analyze(x, opt), profile["files"])),
            anm.struct.UtlFile("Utility.lua"))

        if profile.get("fn_with_fn", True):
            for file in anm_folder.files:
                file.name = self._get_file_name(anm_folder.name, file.name)

        utl_file_path = str(pathlib.Path(
            anm_folder.utl_file.name).with_suffix(""))

        opt.free_lua = anm.struct.F.newline(True).join([
            "ptk = require(\"PSDToolKit\")",
            f"utl = require(\"{utl_file_path}\")"
        ])

        return anm_folder

    def _get_file_name(self, folder_name: str, file_name: str) -> str:
        return "-".join([folder_name, file_name])

    def _get_profile(self, profile_path: str) -> dict:
        profile = json.loads(pathlib.Path(profile_path).read_text("utf-8"))
        jsonschema.validate(profile,
                            json.loads(pathlib.Path(
                                "src/schema/profile.schema.json"
                            ).read_text("utf-8")))
        return profile

    def _anm_analyze(self, anm_profile: dict, opt: anm.struct.ToPTKStrOption) -> anm.struct.AnmFile:
        return anm.struct.AnmFile(
            anm_profile["name"],
            self._tracks_analyze(anm_profile["tracks"],
                                 anm_profile.get("order")),
            opt)

    def _tracks_analyze(self, tracks: list, order: Optional[list]):
        track_names = [truck["name"] for truck in tracks]
        return list(map(lambda x: self._track_analyze(x[0], x[1]),
                        zip(tracks, Sort.get_tracks_order_index(track_names, order))))

    def _track_analyze(self, track: dict, index: int) -> anm.struct.Track:
        track_pos_list = self._rules_analyze(track["rules"])

        return anm.struct.Track(
            track["name"],
            index,
            track.get("always_apply", False),
            Sort.track_pos(self._get_sort_cmp(track.get("sort")), self.checker, track_pos_list))

    def _get_sort_cmp(self, sort: Optional[str]) -> Comparison:
        list: dict[Optional[str], Comparison] = {
            None: lambda x, y: x[0] - y[0],
            "layer:top": lambda x, y: x[0] - y[0],
            "layer:bottom": lambda x, y: y[0] - x[0],
            "key:top": lambda x, y: 0 if x[1].name == y[1].name else 1 if x[1].name < y[1].name else -1,
            "key:bottom": lambda x, y: 0 if x[1].name == y[1].name else -1 if x[1].name < y[1].name else 1
        }
        return list[sort]

    def _rules_analyze(self, rules: list[dict]) -> list[anm.struct.TrackPosition]:
        return RuleChecker.check(self.checker,
                                 list(map(lambda x: self._get_rule(x), rules)))

    def _get_rule(self, _rule) -> rule.Rule:
        if type(_rule) is str:
            return rule.Match(
                rule.Perfect(),
                rule.LayerPathGenerator(),
                _rule)

        if type(_rule) is dict:
            if "ptn" in _rule:
                return rule.Match(
                    self._get_merger(_rule.get("merge")),
                    self._get_generator(_rule.get("func"),
                                        _rule.get("func_params")),
                    _rule["ptn"],
                    self._get_items_value(_rule))
            if "if" in _rule:
                return rule.If(
                    self._get_merger(_rule.get("merge")),
                    rule.LayerPathGenerator(),
                    _rule["if"],
                    self._get_items_value(_rule))

        raise ValueError()

    def _get_items_value(self, _rule):
        if "items" in _rule:
            return rule.LiteralItemsGenerator(_rule["items"])
        if "item_ptns" in _rule:
            return rule.PatternItemsGenerator(_rule["item_ptns"], self.checker)

    def _get_merger(self, merge: Optional[str]) -> rule.Merger:
        return {
            None: rule.Perfect,
            "perfect": rule.Perfect,
            "forward": rule.Forward,
            "backward": rule.Backward,
        }[merge]()

    def _get_generator(self, func: Optional[str], params: dict) -> rule.HitValuesGenerator:
        return {
            None: lambda: rule.LayerPathGenerator(),
            "layer": lambda: rule.LayerPathGenerator(),
            "blink": lambda: rule.BlinkGenerator(params.get("interval", "4"),
                                                 params.get("speed", "1"),
                                                 params.get("offset", "0")),
            "lipsync": lambda: rule.LipsyncGenerator(params.get("speed", "1"),
                                                     params.get("alwaysapply", "true")),
            "anime": lambda: rule.AnimeGenerator(params.get("loffset", "1"),
                                                 params.get("speed", "1"),
                                                 params.get("flip", "true"),
                                                 params.get("loop", "false"),
                                                 params.get("interval", "0")),
        }[func]()
