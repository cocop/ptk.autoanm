import hashlib
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable

from watchdog import events


class WatchingHandler(events.PatternMatchingEventHandler):
    def __init__(self, change: Callable, filter: list[str]):
        super().__init__(filter)
        self.update_list: dict[str, datetime] = {}
        self.change = change

    def _setup_update(self, path):
        self.update_list[str(Path(path).with_suffix(""))]\
            = datetime.fromtimestamp(Path(path).stat().st_mtime)\
            + timedelta(seconds=1)

    def on_created(self, e):
        self._setup_update(e.src_path)

    def on_modified(self, e):
        self._setup_update(e.src_path)

    def on_moved(self, e):
        self._setup_update(e.src_path)

    def check_update_list(self):
        del_list: list[str] = []

        for path, date in self.update_list.items():
            if date <= datetime.now():
                del_list.append(path)
                self.change(path)
                print(path, date)

        for key in del_list:
            del self.update_list[key]


class FileWatchingHandler(events.FileSystemEventHandler):
    def __init__(self, change: Callable[[str], None], file_name):
        super().__init__()
        self.change = change
        self.file_name = file_name
        self.hashes = {}

    def _setup_update(self, path):
        if not Path(path).name.startswith(self.file_name):
            return

        with open(path, 'rb') as f:
            checksum = hashlib.md5(f.read()).hexdigest()

        if path not in self.hashes or (self.hashes[path] != checksum):
            self.hashes[path] = checksum
            self.change(str(path))

    def on_created(self, e):
        self._setup_update(e.src_path)

    def on_modified(self, e):
        self._setup_update(e.src_path)

    def on_moved(self, e):
        self._setup_update(e.src_path)
