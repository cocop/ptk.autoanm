import itertools
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Optional

from .format import Format as F

# ========================================


class ToPTKStrOption:
    def __init__(self,
                 free_lua: str,
                 layer_prefix: str,
                 ptk_req: str,
                 utl_req: str) -> None:
        self.free_lua = free_lua
        self.layer_prefix = layer_prefix
        self.ptk_req = ptk_req
        self.utl_req = utl_req


class OutObj(ABC):
    @abstractmethod
    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        pass


# ========================================


class Presenter(OutObj):
    @abstractmethod
    def get_symbol_layer(self) -> str:
        pass


class LayerPath(Presenter):
    def __init__(self, path: str) -> None:
        self.path = path

    def get_symbol_layer(self) -> str:
        return self.path

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        return "".join([F.indent(indent, depth), '"', opt.layer_prefix,  self.path, '"'])


class Lipsync(Presenter):
    def __init__(self, items: list[str], speed: str, alwaysapply: str) -> None:
        self.items = items
        self.speed = speed
        self.alwaysapply = alwaysapply

    def get_symbol_layer(self) -> str:
        return self.items[0]

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        return "".join([
            F.indent(indent, depth),
            opt.ptk_req,
            ".LipSyncSimple.new({",
            F.newline(indent != 0),
            F.indent(indent, depth + 1),
            '"',
            ('",' +
             F.newline(indent != 0) +
             F.indent(indent, depth + 1) +
             '"').join(map(lambda x: opt.layer_prefix + x, self.items)),
            '"',
            F.newline(indent != 0),
            F.indent(indent, depth),
            "},",
            F.space(indent != 0),
            self.speed,
            ",",
            F.space(indent != 0),
            self.alwaysapply,
            ")"
        ])


class Blink(Presenter):
    def __init__(self, items: list[str], interval: str, speed: str, offset: str) -> None:
        self.items = items
        self.interval = interval
        self.speed = speed
        self.offset = offset

    def get_symbol_layer(self) -> str:
        return self.items[-1]

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        return "".join([
            F.indent(indent, depth),
            opt.ptk_req,
            ".Blinker.new({",
            F.newline(indent != 0),
            F.indent(indent, depth + 1),
            '"',
            ('",' +
             F.newline(indent != 0) +
             F.indent(indent, depth + 1) +
             '"').join(map(lambda x: opt.layer_prefix + x, self.items)),
            '"',
            F.newline(indent != 0),
            F.indent(indent, depth),
            "},",
            F.space(indent != 0),
            self.interval,
            ",",
            F.space(indent != 0),
            self.speed,
            ",",
            F.space(indent != 0),
            self.offset,
            ")"
        ])


class Anime(Presenter):
    def __init__(self, items: list[str], offset: str, speed: str, flip: str, loop: str, interval: str) -> None:
        self.items = items
        self.offset = offset
        self.speed = speed
        self.flip = flip
        self.loop = loop
        self.interval = interval

    def get_symbol_layer(self) -> str:
        return self.items[-1]

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        return "".join([
            F.indent(indent, depth),
            opt.utl_req,
            ":anime({",
            F.newline(indent != 0),
            F.indent(indent, depth + 1),
            '"',
            ('",' +
             F.newline(indent != 0) +
             F.indent(indent, depth + 1) +
             '"').join(map(lambda x: opt.layer_prefix + x, self.items)),
            '"',
            F.newline(indent != 0),
            F.indent(indent, depth),
            "},",
            F.space(indent != 0),
            self.offset,
            ",",
            F.space(indent != 0),
            self.speed,
            ",",
            F.space(indent != 0),
            self.flip,
            ",",
            F.space(indent != 0),
            self.loop,
            ",",
            F.space(indent != 0),
            self.interval,
            ")"
        ])


# ========================================


class TrackPosition(OutObj):
    def __init__(self, name: str, items: list[Presenter]) -> None:
        super().__init__()
        self.name = name
        self.items = items

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        if len(self.items) == 1:
            return self.items[0].get_psdtoolkit_str(opt, indent, depth)

        value_strs = list(map(lambda x: x.get_psdtoolkit_str(opt, indent, depth + 1),
                              self.items))
        items_space = ("," + F.newline(indent != 0)).join(value_strs)

        if indent != 0 and len(items_space) > indent * depth:
            items_space_list = list(items_space)
            items_space_list[indent * depth] = "{"
            items_space = "".join(items_space_list)
            return "".join([
                items_space,
                "}"
            ])

        return "".join([
            "{",
            items_space,
            "}",
        ])


class Track(OutObj):
    def __init__(self, name: str, index: int, always_apply: bool, values: list[TrackPosition]) -> None:
        super().__init__()
        self.name = name
        self.index = index
        self.always_apply = always_apply
        self.values = values

    def get_psdtoolkit_str(self, opt: ToPTKStrOption, indent: int, depth: int) -> str:
        values_space = map(lambda x: x.get_psdtoolkit_str(opt, indent, depth + 1),
                           self.values)

        return "".join([
            opt.utl_req,
            ":addstates({",
            F.newline(indent != 0),
            ("," + F.newline(indent != 0)).join(values_space),
            F.newline(indent != 0),
            "},",
            F.space(indent != 0),
            "obj.track",
            str(self.index),
            ",",
            F.space(indent != 0),
            str(self.index),
            ")"
        ])


# ========================================
class OutFile(ABC):
    name: str

    @abstractmethod
    def get_psdtoolkit_str(self, indent: int = 0) -> str:
        pass


class UtlFile(OutFile):
    def __init__(self, name: str) -> None:
        self.name = name

    def get_psdtoolkit_str(self, indent: int = 0) -> str:
        return Path("src/lua/Utility.lua").read_text()


class AnmFile(OutFile):
    def __init__(self, name: str, tracks: list[Track], opt: ToPTKStrOption) -> None:
        self.name = name
        self.tracks = tracks
        self.opt = opt

    def get_psdtoolkit_str(self, indent: int = 0) -> str:
        tracks_space = map(lambda x: x.get_psdtoolkit_str(self.opt, indent, 0),
                           self.tracks)

        tracks = itertools.chain.from_iterable([
            [
                "--track",
                str(track.index),
                ":",
                track.name,
                ",",
                "1" if track.always_apply else "0",
                ",",
                str(len(track.values)),
                ",",
                "1" if track.always_apply else "0",
                ",1",
                F.newline(indent != 0),
            ]
            for track in sorted(self.tracks, key=lambda x: x.index)])

        return "".join([
            "".join(tracks),
            "".join(self.opt.free_lua),
            F.newline(indent != 0) * 2,
            (F.newline(indent != 0) * 2).join(tracks_space),
            F.newline(indent != 0) * 2,
            self.opt.utl_req,
            ":register(", str(len(self.tracks)), ")"
        ])


class AnmFolder:
    def __init__(self, name: str, psd_copy_path: Optional[str], dist_path: str, anm_files: list[AnmFile], utl_file: UtlFile) -> None:
        self.name = name
        self.psd_copy_path = psd_copy_path
        self.dist_path = dist_path
        self.anm_files = anm_files
        self.utl_file = utl_file

    @property
    def files(self) -> list[OutFile]:
        r: list[OutFile] = [x for x in self.anm_files]
        r.append(self.utl_file)
        return r

# ========================================
