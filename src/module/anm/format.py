class Format:
    @staticmethod
    def space(is_space: bool):
        return " " if is_space else ""

    @staticmethod
    def newline(is_newline: bool):
        return "\n" if is_newline else ""

    @staticmethod
    def indent(indent: int, depth: int):
        return " " * (indent * depth)
