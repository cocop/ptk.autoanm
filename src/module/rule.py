import enum
import re
from abc import ABC, abstractmethod
from typing import Optional, Tuple

from module import layer

from .anm import struct

# ================================


class HitValues():
    def __init__(self, name: str, values: list[struct.Presenter]) -> None:
        self.name = name
        self.values = values


# ================================


class Merger(ABC):
    @ abstractmethod
    def merge(self, dst:  dict[str, list[struct.Presenter]], src: HitValues):
        pass


class Perfect(Merger):
    def merge(self, dst:  dict[str, list[struct.Presenter]], src: HitValues):
        if not src.name in dst:
            dst[src.name] = src.values
            return

        dst[src.name].extend(src.values)


class Forward(Merger):
    def merge(self, dst:  dict[str, list[struct.Presenter]], src: HitValues):
        is_merged = False

        for key in dst.keys():
            if key.startswith(src.name):
                is_merged = True
                dst[key].extend(src.values)

        if not is_merged:
            dst[src.name] = src.values


class Backward(Merger):
    def merge(self, dst:  dict[str, list[struct.Presenter]], src: HitValues):
        is_merged = False

        for key in dst.keys():
            if key.endswith(src.name):
                is_merged = True
                dst[key].extend(src.values)

        if not is_merged:
            dst[src.name] = src.values


# ================================


class HitValuesGenerator(ABC):
    @ abstractmethod
    def create(self, name: str, items: list[str]) -> HitValues:
        pass


class LayerPathGenerator(HitValuesGenerator):
    def create(self, name: str, items: list[str]):
        return HitValues(name,
                         list(map(lambda x: struct.LayerPath(x), items)))


class LipsyncGenerator(HitValuesGenerator):
    def __init__(self, speed: str, alwaysapply: str):
        self.speed = speed
        self.alwaysapply = alwaysapply

    def create(self, name: str, items: list[str]):
        if len(items) < 2:
            raise ValueError(name + " でレイヤーが最低必要数見つかりませんでした")

        return HitValues(name,
                         [struct.Lipsync(items, self.speed, self.alwaysapply)])


class BlinkGenerator(HitValuesGenerator):
    def __init__(self, interval: str, speed: str, offset: str):
        self.interval = interval
        self.speed = speed
        self.offset = offset

    def create(self, name: str, items: list[str]):
        if len(items) < 2:
            raise ValueError(name + " でレイヤーが最低必要数見つかりませんでした")

        return HitValues(name,
                         [struct.Blink(items, self.interval, self.speed, self.offset)])


class AnimeGenerator(HitValuesGenerator):
    def __init__(self, offset: str, speed: str, flip: str, loop: str, interval: str):
        self.offset = offset
        self.speed = speed
        self.loop = loop
        self.flip = flip
        self.interval = interval

    def create(self, name: str, items: list[str]):
        if len(items) < 2:
            raise ValueError(name + " でレイヤーが最低必要数見つかりませんでした")

        # 先頭に "!" があった場合は正規表現でレイヤーを探す
        offset = self.offset
        if offset[0] == "!":
            offset = self.get_offset(name, offset[1:-1], items)

        return HitValues(name,
                         [struct.Anime(items,  offset, self.speed, self.flip, self.loop, self.interval)])

    def get_offset(self, name: str, pattern: str, items: list[str]):
        for idx, item in enumerate(items):
            if re.match(pattern, item) != None:
                return str(idx + 1)

        raise ValueError(name + " で offsetレイヤーが見つかりませんでした")


# ================================


class ItemsGenerator(ABC):
    def items_replace(self, items: list[str], keys: list[str]):
        rep_list = self._get_replace_list(keys)
        result: list[str] = []

        for item in items:
            result.append(self._item_replace(item, rep_list))

        return result

    def _get_replace_list(self, keys: list[str]) -> dict[str, str]:
        result = dict(map(self._get_kvpair, enumerate(keys)))
        result["{@}"] = keys[0]
        return result

    def _get_kvpair(self, kv: Tuple[int, str]):
        (key, value) = kv
        return ("".join(["{@", str(key), "}"]), value)

    def _item_replace(self, item: str, rep_list: dict[str, str]):
        for key, value in rep_list.items():
            item = item.replace(key, value)
        return item

    @ abstractmethod
    def create(self, keys: Optional[list[str]]) -> list[str]:
        pass


class LiteralItemsGenerator(ItemsGenerator):
    def __init__(self, items: list[str]):
        self.items = items

    def create(self, keys: Optional[list[str]]):
        if keys == None:
            return self.items

        return self.items_replace(self.items, keys)


class PatternItemsGenerator(ItemsGenerator):
    def __init__(self, ptns: list[str], checker: layer.Checker):
        self.ptns = ptns
        self.checker = checker

    def create(self, keys: Optional[list[str]]):
        ptns = self.ptns
        if keys != None:
            keys = self._escape_regex_list(keys)
            ptns = self.items_replace(self.ptns, keys)

        return self.checker.check(MatchReceiver(ptns))

    def _escape_regex_list(self, src_ls: list[str]):
        return list(map(lambda src: self._escape_regex(src), src_ls))

    def _escape_regex(self, src: str):
        keywords = "\\.^$*+?}{[]|()-"

        for keyword in keywords:
            src = src.replace(keyword, "\\" + keyword)
        return src


# ================================


class Target(enum.Enum):
    File = 0
    Result = 1


class Rule(ABC):
    target: Target
    merger: Merger

    @ abstractmethod
    def check(self, key: str) -> Optional[HitValues]:
        pass


class Match(Rule):
    def __init__(self, merger: Merger, hit_val_gen: HitValuesGenerator, pattern: str, items_gen: Optional[ItemsGenerator] = None) -> None:
        super().__init__()
        self.target = Target.File
        self.merger = merger
        self._hit_val_gen = hit_val_gen
        self._pattern = pattern.replace("{@}", "[^/]*?")
        self._items_gen = items_gen

    def check(self, key: str) -> Optional[HitValues]:
        m = re.match(self._pattern, key)
        if m == None:
            return None

        tpos_strs = list(m.groups())
        if len(tpos_strs) == 0:
            return None
        # ここまでマッチチェック

        tpos_name = "/".join(tpos_strs)

        if (self._items_gen == None):
            return self._hit_val_gen.create(tpos_name, [key])

        return self._hit_val_gen.create(tpos_name,
                                        self._items_gen.create(tpos_strs))


class If(Rule):
    def __init__(self, merger: Merger, hit_val_gen: HitValuesGenerator, conditions: str, items_gen: Optional[ItemsGenerator] = None) -> None:
        super().__init__()
        self.target = Target.Result
        self.merger = merger
        self._hit_val_gen = hit_val_gen
        self._conditions = conditions
        self._items_gen = items_gen

    def check(self, key: str) -> Optional[HitValues]:
        m = re.match(self._conditions, key)
        if m == None:
            return None

        if self._items_gen == None:
            raise ValueError("if ルールにitemsが存在しません")

        return self._hit_val_gen.create(key, self._items_gen.create(None))


# ================================


class RuleReceiver(layer.Receiver[dict[str, list[struct.Presenter]]]):
    def __init__(self, result: dict[str, list[struct.Presenter]], rule: Rule):
        self.result = result
        self.rule = rule

    def catch(self, path: list[str]) -> layer.NextCheck:
        hit_values = self.rule.check("/".join(path))

        if hit_values == None:
            return layer.NextCheck.Next

        self.rule.merger.merge(self.result, hit_values)
        return layer.NextCheck.DirSkip

    def get_result(self) -> dict[str, list[struct.Presenter]]:
        return self.result


class AllLayerReceiver(layer.Receiver[list[str]]):
    def __init__(self):
        self.result: list[str] = []

    def catch(self, path: list[str]) -> layer.NextCheck:
        self.result.append("/".join(path))
        return layer.NextCheck.Next

    def get_result(self):
        return self.result


class MatchReceiver(layer.Receiver[list[str]]):
    def __init__(self, ptns: Optional[list[str]]):
        self.result: list[str] = []
        self.ptns = ptns

    def catch(self, path: list[str]) -> layer.NextCheck:
        if self.ptns == None:
            return layer.NextCheck.End

        str_path = "/".join(path)
        for ptn in self.ptns:
            if re.match(ptn, str_path) != None:
                self.result.append(str_path)
                return layer.NextCheck.DirSkip

        return layer.NextCheck.Next

    def get_result(self):
        return self.result
